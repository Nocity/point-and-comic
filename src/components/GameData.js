import { markRaw } from 'vue'

export let gameData = {
  panelData: [
    {image: "white-void/Intro", advance: [85], pos: {x:1, y:-1}, env: 'WhiteVoid',                // 0
      interactionComponent: 'DeleteSave'},
    {image: "white-void/Panel2", advance: [2], pos: {x:2, y:0}, env: 'WhiteVoid'},                // 1
    {image: "white-void/Panel3", advance: [3], pos: {x:3, y:0}, env: 'WhiteVoid'},                // 2
    {image: "white-void/Panel4", advance: [4], pos: {x:4, y:0}, env: 'WhiteVoid'},                // 3
    {image: "white-void/Panel5", advance: [5], pos: {x:5, y:0}, env: 'WhiteVoid',                 // 4
      interactionComponent: 'RedDoor'},
    {image: "redland/Panel1", advance: [6], pos: {x:6, y:0}, env: 'RedLand',                      // 5
      startsLocked: true},
    {image: "redland/Panel2", advance: [7], pos: {x:7, y:0}, env: 'RedLand'},                     // 6
    {image: "redland/Panel3", advance: [8, 16], pos: {x:8, y:0}, env: 'RedLand'},                 // 7
    {image: "redland/Panel4", advance: [9], pos: {x:9, y:0}, env: 'RedLand'},                     // 8
    {image: "redland/Panel5", advance: [10], pos: {x:10, y:0}, env: 'RedLand'},                   // 9
    {image: "redland/Panel6", advance: [11], pos: {x:11, y:0}, env: 'RedLand'},                   // 10
    {image: "redland/Panel7", advance: [12], pos: {x:12, y:0}, env: 'RedLand'},                   // 11
    {image: "redland/Panel8", advance: [13], pos: {x:13, y:0}, env: 'RedLand',                    // 12
      interactionComponent: 'RedDarkness'},
    {image: "redland/Panel9", advance: [14], pos: {x:14, y:0}, env: 'RedLand',                    // 13
      startsLocked: true},
    {image: "redland/Panel10", advance: [15], pos: {x:15, y:0}, env: 'RedLand'},                  // 14
    {image: "redland/Panel11", advance: [21], pos: {x:16, y:0}, env: 'RedLand'},                  // 15
    {image: "redland/Ladder1", advance: [17], pos: {x:8, y:-1}, env: 'RedLand'},                  // 16
    {image: "redland/Ladder2", advance: [18], pos: {x:8, y:-2}, env: 'RedLand'},                  // 17
    {image: "redland/Ladder3", advance: [19], pos: {x:9, y:-2}, env: 'RedLand'},                  // 18
    {image: "redland/Ladder4", advance: [20], pos: {x:10, y:-2}, env: 'RedLand'},                 // 19
    {image: "redland/Ladder5", advance: [], pos: {x:11, y:-2}, env: 'RedLand',                    // 20
      interactionComponent: 'LanternTake'},
    {image: "twin-roads/StairsIn1", advance: [22], pos: {x:17, y:0}, env: 'TwinRoadsStairs'},     // 21
    {image: "twin-roads/StairsIn2", advance: [23], pos: {x:18, y:0}, env: 'TwinRoadsStairs'},     // 22
    {image: "twin-roads/StairsIn3", advance: [24], pos: {x:19, y:0}, env: 'TwinRoadsStairs'},     // 23
    {image: "twin-roads/StairsIn4", advance: [25, 28], pos: {x:20, y:0}, env: 'TwinRoadsStairs',  // 24
      sound: "Multicreep"},
    {image: "twin-roads/StairsInUp1", advance: [26], pos: {x:20, y:-1}, env: 'TwinRoadsStairs'},  // 25
    {image: "twin-roads/StairsInUp2", advance: [27], pos: {x:20, y:-2}, env: 'TwinRoadsStairs'},  // 26
    {image: "twin-roads/StairsInUp3", advance: [31], pos: {x:21, y:-2}, env: 'TwinRoadsStairs'},  // 27
    {image: "twin-roads/StairsInDown1", advance: [29], pos: {x:20, y:1}, env: 'TwinRoadsStairs'}, // 28
    {image: "twin-roads/StairsInDown2", advance: [30], pos: {x:20, y:2}, env: 'TwinRoadsStairs'}, // 29
    {image: "twin-roads/StairsInDown3", advance: [40], pos: {x:21, y:2}, env: 'TwinRoadsStairs'}, // 30
    {image: "twin-roads/Upper1", advance: [32], pos: {x:22, y:-2}, env: 'TwinRoadsUpper'},        // 31
    {image: "twin-roads/Upper2", advance: [33], pos: {x:23, y:-2}, env: 'TwinRoadsUpper'},        // 32
    {image: "twin-roads/Upper3", advance: [34], pos: {x:24, y:-2}, env: 'TwinRoadsUpper',         // 33
      interactionComponent: "PlankGap"},
    {image: "twin-roads/Upper4", advance: [35], pos: {x:25, y:-2}, env: 'TwinRoadsUpper',         // 34
      startsLocked: true},
    {image: "twin-roads/Upper5", advance: [36], pos: {x:26, y:-2}, env: 'TwinRoadsUpper'},        // 35
    {image: "twin-roads/Upper6", advance: [37],  pos: {x:27, y:-2}, env: 'TwinRoadsUpper'},       // 36
    {image: "twin-roads/Upper7", advance: [38], pos: {x:28, y:-2}, env: 'TwinRoadsUpper',         // 37
      interactionComponent: 'HandsStatue'},
    {image: "twin-roads/Upper8", advance: [39], pos: {x:29, y:-2}, env: 'TwinRoadsUpper'},        // 38
    {image: "twin-roads/Upper9", advance: [49], pos: {x:30, y:-2}, env: 'TwinRoadsUpper'},        // 39
    {image: "twin-roads/Lower1", advance: [41], pos: {x:22, y:2}, env: 'TwinRoadsLower'},         // 40
    {image: "twin-roads/Lower2", advance: [42], pos: {x:23, y:2}, env: 'TwinRoadsLower'},         // 41
    {image: "twin-roads/Lower3", advance: [43], pos: {x:24, y:2}, env: 'TwinRoadsLower'},         // 42
    {image: "twin-roads/Lower4", advance: [44], pos: {x:25, y:2}, env: 'TwinRoadsLower',          // 43
      interactionComponent: "RopeTake"},
    {image: "twin-roads/Lower5", advance: [45], pos: {x:26, y:2}, env: 'TwinRoadsLower'},         // 44
    {image: "twin-roads/Lower6", advance: [46], pos: {x:27, y:2}, env: 'TwinRoadsLower'},         // 45
    {image: "twin-roads/Lower7", advance: [47], pos: {x:28, y:2}, env: 'TwinRoadsLower',          // 46
      interactionComponent: "PlankTake"},
    {image: "twin-roads/Lower8", advance: [48], pos: {x:29, y:2}, env: 'TwinRoadsLower'},         // 47
    {image: "twin-roads/Lower9", advance: [52], pos: {x:30, y:2}, env: 'TwinRoadsLower'},         // 48
    {image: "twin-roads/StairsOutDown1", advance: [50], pos: {x:31, y:-2}, env: 'TwinRoadsStairs'},// 49
    {image: "twin-roads/StairsOutDown2", advance: [51], pos: {x:32, y:-2}, env: 'TwinRoadsStairs'},// 50
    {image: "twin-roads/StairsOutDown3", advance: [55], pos: {x:32, y:-1}, env: 'TwinRoadsStairs'},// 51
    {image: "twin-roads/StairsOutUp1", advance: [53], pos: {x:31, y:2}, env: 'TwinRoadsStairs'},  // 52
    {image: "twin-roads/StairsOutUp2", advance: [54], pos: {x:32, y:2}, env: 'TwinRoadsStairs'},  // 53
    {image: "twin-roads/StairsOutUp3", advance: [55], pos: {x:32, y:1}, env: 'TwinRoadsStairs'},  // 54
    {image: "twin-roads/StairsOut1", advance: [56], pos: {x:32, y:0}, env: 'TwinRoadsStairs',     // 55
      sound: "Multicreep"},
    {image: "twin-roads/StairsOut2", advance: [57], pos: {x:33, y:0}, env: 'TwinRoadsStairs'},    // 56
    {image: "twin-roads/StairsOut3", advance: [58], pos: {x:34, y:0}, env: 'TwinRoadsStairs'},    // 57
    {image: "twin-roads/StairsOut4", advance: [59], pos: {x:35, y:0}, env: 'TwinRoadsStairs'},    // 58
    {image: "twin-roads/StairsOut5", advance: [60], pos: {x:36, y:0}, env: 'TwinRoadsStairs'},    // 59
    {image: "twin-roads/StairsOut6", advance: [61], pos: {x:37, y:0}, env: 'TwinRoadsStairs'},    // 60
    {image: "twin-roads/StairsOut7", advance: [62], pos: {x:38, y:0}, env: 'TwinRoadsStairs',     // 61
      interactionComponent: "KeyTake"},
    {image: "twin-roads/Pit1", advance: [63], pos: {x:39, y:0}, env: 'TwinRoadsStairs'},          // 62
    {image: "twin-roads/Pit2", advance: [64], pos: {x:40, y:0}, env: 'TwinRoadsStairs'},          // 63
    {image: "twin-roads/Pit3", advance: [65, 70, 75], pos: {x:41, y:0}, env: 'TwinRoadsStairs',   // 64
      interactionComponent: "GapWatcher"},
    {image: "twin-roads/PitFailRope1", advance: [66], pos: {x:41, y:1}, env: 'TwinRoadsStairs'},  // 65
    {image: "twin-roads/PitFailRope2", advance: [67], pos: {x:41, y:2}, env: 'TwinRoadsStairs'},  // 66
    {image: "twin-roads/PitFailRope3", advance: [68], pos: {x:40, y:2}, env: 'TwinRoadsStairs'},  // 67
    {image: "twin-roads/PitFailRope4", advance: [69], pos: {x:39, y:2}, env: 'TwinRoadsStairs',   // 68
      sound: "Thinking"},
    {image: "twin-roads/PitFailRope5", advance: [62], pos: {x:39, y:1}, env: 'TwinRoadsStairs'},  // 69
    {image: "twin-roads/PitFailPlank1", advance: [71], pos: {x:41, y:-1}, env: 'TwinRoadsStairs'},// 70
    {image: "twin-roads/PitFailPlank2", advance: [72], pos: {x:41, y:-2}, env: 'TwinRoadsStairs'},// 71
    {image: "twin-roads/PitFailPlank3", advance: [73], pos: {x:40, y:-2}, env: 'TwinRoadsStairs'},// 72
    {image: "twin-roads/PitFailPlank4", advance: [74], pos: {x:39, y:-2}, env: 'TwinRoadsStairs', // 73
      sound: "Thinking"},
    {image: "twin-roads/PitFailPlank5", advance: [62], pos: {x:39, y:-1}, env: 'TwinRoadsStairs'},// 74
    {image: "twin-roads/Pit4", advance: [76], pos: {x:42, y:0}, env: 'TwinRoadsStairs',           // 75
      startsLocked: true, interactionComponent: "GapCross"},
    {image: "twin-roads/Pit5", advance: [77], pos: {x:43, y:0}, env: 'TwinRoadsStairs'},          // 76
    {image: "twin-roads/Pit6", advance: [78], pos: {x:44, y:0}, env: 'TwinRoadsStairs'},          // 77
    {image: "twin-roads/Pit7", advance: [79], pos: {x:45, y:0}, env: 'TwinRoadsStairs'},          // 78
    {image: "twin-roads/Pit8", advance: [80], pos: {x:46, y:0}, env: 'TwinRoadsStairs'},          // 79
    {image: "twin-roads/Pit9", advance: [81], pos: {x:47, y:0}, env: 'TwinRoadsStairs',           // 80
      interactionComponent: "FinalLock"},
    {image: "twin-roads/EndOfDemo", advance: [], pos: {x:48, y:0}, env: 'TwinRoadsStairs',        // 81
      startsLocked: true},
    {image: "white-void/PanelBackwards1", advance: [83], pos: {x:0, y:0}, env: 'WhiteVoid',       // 82
      startsHidden: true},
    {image: "white-void/PanelBackwards2", advance: [84], pos: {x:-1, y:0}, env: 'WhiteVoid'},     // 83
    {image: "white-void/PanelBackwards3", advance: [], pos: {x:-2, y:0}, env: 'WhiteVoid',        // 84
      interactionComponent: "StoneTake"},
    {image: "white-void/Panel1", advance: [1, 82], pos: {x:1, y:0}, env: 'WhiteVoid'},            // 85
  ],
  environmentData: [
    {name: 'Default', music: '', color: '#808080'},
    {name: 'WhiteVoid', music: 'VoidAmbience', color: '#808080'},
    {name: 'RedLand', music: 'RedPath', color: '#a83838'},
    {name: 'TwinRoadsStairs', music: 'StairsDuo', color: '#307f90'},
    {name: 'TwinRoadsUpper', music: 'UpperTwin', color: '#a85338'},
    {name: 'TwinRoadsLower', music: 'LowerTwin', color: '#903074'}
  ],
  sfxData: [
    {name: 'DoorOpen', volume: .5},
    {name: 'InteractionFail', volume: .5},
    {name: 'LightLantern', volume: .5},
    {name: 'Locked', volume: .45},
    {name: 'Multicreep', volume: .27},
    {name: 'NavigatePanel', volume: .35},
    {name: 'PutItem', volume: .5},
    {name: 'SelectItem', volume: .5},
    {name: 'StoneSlide', volume: .5},
    {name: 'TakeItem', volume: .4},
    {name: 'Thinking', volume: .75}
  ],
  musicData: [
    {name: 'VoidAmbience', volume: .4},
    {name: 'RedPath', volume: .9},
    {name: 'StairsDuo', volume: .7},
    {name: 'UpperTwin', volume: 1},
    {name: 'LowerTwin', volume: .9}
  ],
  gameStateDefaults: {
    // reserved name: panelState
    inventory: [],
    lanternTaken: false,
    ropeTaken: false,
    plankTaken: false,
    keyTaken: false,
    stoneTaken: false,
    hasCrossedGap: false,
    handsStatue: {leftFill: 'Bow', rightFill: 'Arrow'}
  }
}

export default {
  name: 'GameData',
  data() {
    return {gameData: markRaw(gameData)}
  }
}

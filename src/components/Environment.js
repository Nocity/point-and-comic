import { gameData } from '@/components/GameData.js'
import { musicPlayer } from '@/components/AudioPlayer.js'

let environmentState = {
  currentEnvironment: JSON.parse(JSON.stringify(gameData.environmentData[0]))
}

export let EnvironmentControls = {
  name: 'EnvironmentControls',
  data() {
    return environmentState
  },
  methods: {
    setEnvironment(envName) {
      if(envName == '') {
        envName = 'Default'
      }
      let newEnv = gameData.environmentData.find((entry) =>
        entry.name == envName
      )
      //this.currentEnvironment = newEnv
      this.currentEnvironment.color = newEnv.color
      if(newEnv.music != '')
        musicPlayer.play(newEnv.music)
      else
        musicPlayer.stop()
    },
    setEnvironmentFar() {
      musicPlayer.changeVolume(.65)
    }
  }
}

export default {
  name: 'Environment',
  mixins: [
    EnvironmentControls
  ]
}

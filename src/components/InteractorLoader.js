import RedDoor from '@/components/interactors/RedDoor.vue'
import LanternTake from '@/components/interactors/LanternTake.vue'
import RedDarkness from '@/components/interactors/RedDarkness.vue'
import PlankGap from '@/components/interactors/PlankGap.vue'
import HandsStatue from '@/components/interactors/HandsStatue.vue'
import GapWatcher from '@/components/interactors/GapWatcher.vue'
import GapCross from '@/components/interactors/GapCross.vue'
import FinalLock from '@/components/interactors/FinalLock.vue'
import KeyTake from '@/components/interactors/KeyTake.vue'
import PlankTake from '@/components/interactors/PlankTake.vue'
import RopeTake from '@/components/interactors/RopeTake.vue'
import StoneTake from '@/components/interactors/StoneTake.vue'
import DeleteSave from '@/components/interactors/DeleteSave.vue'

let interactors = {
  RedDoor: RedDoor,
  LanternTake: LanternTake,
  RedDarkness: RedDarkness,
  PlankGap: PlankGap,
  HandsStatue: HandsStatue,
  GapWatcher: GapWatcher,
  GapCross: GapCross,
  FinalLock: FinalLock,
  KeyTake: KeyTake,
  PlankTake: PlankTake,
  RopeTake: RopeTake,
  StoneTake: StoneTake,
  DeleteSave: DeleteSave
}

export default {
  name: 'InteractorLoader',
  methods: {
    fetchInteractor(name) {
      return interactors[name]
    }
  }
}

import { gameData } from '@/components/GameData.js'

let defaultState = {
  panelState: (function() {
    let initialPanelState = []
    for(let i in gameData.panelData) {
      initialPanelState.push({
        discovered: i == 0,
        locked: gameData.panelData[i].startsLocked,
        hidden: gameData.panelData[i].startsHidden
      })
    }
    return initialPanelState
  })()
}

for(let key in gameData.gameStateDefaults) {
  defaultState[key] = gameData.gameStateDefaults[key]
}

let gameState = {
  clear() {
    for(let key in defaultState) {
      this[key] = JSON.parse(JSON.stringify(defaultState[key]))
    }
  }
}

gameState.clear()

export default {
  name: 'GameState',
  data() {
    return {gameState: gameState}
  }
}

let noWatch = false

export let SaveWatcher = {
  name: 'SaveWatcher',
  data() {
    return {gameState: gameState}
  },
  created() {
    if(localStorage.getItem('saveGame') === null) {
      localStorage.saveGame = '{}'
    }
    let saveGame = JSON.parse(localStorage.saveGame)
    noWatch = true
    for(let key in saveGame) {
      this.gameState[key] = saveGame[key]
    }
    noWatch = false
  },
  watch: {
    gameState: {
      handler() {
        if(noWatch) {
          return
        }
        localStorage.saveGame = JSON.stringify(this.gameState)
      },
      deep: true
    }
  }
}

import { gameData } from '@/components/GameData'

let sfxAudioPlayers = {}

for(let key in gameData.sfxData) {
  let sfxName = gameData.sfxData[key].name
  let audio = new Audio(require('@/assets/snd/sfx/' + sfxName + '.wav'))
  audio.volume = gameData.sfxData[key].volume
  sfxAudioPlayers[sfxName] = audio
}

let musicAudioPlayers = {}
let activeMusicName = ''

for(let key in gameData.musicData) {
  let musicName = gameData.musicData[key].name
  let audio = new Audio(require('@/assets/snd/music/' + musicName + '.ogg'))
  audio.mixingVolume = audio.volume = gameData.musicData[key].volume
  audio.loop = true
  musicAudioPlayers[musicName] = audio
}

export let sfxPlayer = {
  play(sfxName) {
    sfxAudioPlayers[sfxName].pause()
    sfxAudioPlayers[sfxName].currentTime = 0
    sfxAudioPlayers[sfxName].play()
  },
  stop(sfxName) {
    sfxAudioPlayers[sfxName].pause()
    sfxAudioPlayers[sfxName].currentTime = 0
  }
}

export let musicPlayer = {
  play(newMusicName) {
    if(activeMusicName == newMusicName) {
      this.changeVolume(1)
      return;
    }
    clearTransition()
    clearFadingMusic()
    if(activeMusicName != '') {
      let oldMusicName = activeMusicName
      activeMusicName = newMusicName
      fadeOut(oldMusicName, () => musicAudioPlayers[newMusicName].play())
    }
    else {
      activeMusicName = newMusicName
      musicAudioPlayers[newMusicName].play()
    }
  },
  changeVolume(newVol) {
    if(activeMusicName != '') {
      clearTransition()
      transitionVolume(activeMusicName, newVol*musicAudioPlayers[activeMusicName].mixingVolume)
    }
  },
  stop() {
    clearTransition()
    clearFadingMusic()
    if(activeMusicName != '') {
      let oldMusicName = activeMusicName
      activeMusicName = ''
      fadeOut(oldMusicName)
    }
  },
  getActiveMusic() {
    return activeMusicName
  }
}

let isFadeActive = false
let fadedMusicName
let activeFadeInterval

function fadeOut(musicName, callback) {
  let mixingVolume = musicAudioPlayers[musicName].mixingVolume
  isFadeActive = true
  fadedMusicName = musicName
  activeFadeInterval = setInterval(function() {
    let newVol = musicAudioPlayers[musicName].volume - .08*mixingVolume
    if(newVol <= 0) {
      clearFadingMusic()
      if(callback != undefined) {
        callback()
      }
    }
    else {
      musicAudioPlayers[musicName].volume = newVol
    }
  }, 50)
}

function clearFadingMusic() {
  if(isFadeActive) {
    musicAudioPlayers[fadedMusicName].pause()
    musicAudioPlayers[fadedMusicName].currentTime = 0
    musicAudioPlayers[fadedMusicName].volume = musicAudioPlayers[fadedMusicName].mixingVolume
    clearInterval(activeFadeInterval)
    isFadeActive = false
  }
}

let isTransitionActive = false
let activeTransitionInterval

function transitionVolume(musicName, targetVolume) {
  let mixingVol = musicAudioPlayers[musicName].mixingVolume
  let stepSign = Math.sign(targetVolume - musicAudioPlayers[musicName].volume)
  isTransitionActive = true
  activeTransitionInterval = setInterval(function() {
    let newVol = musicAudioPlayers[musicName].volume + stepSign*mixingVol*.08
    if(Math.sign(targetVolume - newVol) != stepSign) {
      musicAudioPlayers[musicName].volume = targetVolume
      clearTransition()
    }
    else {
      musicAudioPlayers[musicName].volume = newVol
    }
  }, 50)
}

function clearTransition() {
  if(isTransitionActive) {
    clearInterval(activeTransitionInterval)
    isTransitionActive = false
  }
}

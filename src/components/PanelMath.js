export default {
  name: 'PanelMath',
  methods: {
    panelSize() {
      return Math.min(window.innerWidth, window.innerHeight)*.6
    },
    unitSize() {
      return Math.min(window.innerWidth, window.innerHeight)*.72
    },
    centerPanelOffset(scale) {
      return {x: window.innerWidth/2 - this.panelSize()/2*scale,
              y: window.innerHeight/2 - this.panelSize()/2*scale}
    },
    panelsToPixels(p) {
      return p * this.panelSize()
    },
    unitsToPixels(p) {
      return p * this.unitSize()
    },
    panelsAABB(panels) {
      let aabb = {left: Number.POSITIVE_INFINITY,
                  right: Number.NEGATIVE_INFINITY,
                  top: Number.POSITIVE_INFINITY,
                  bottom: Number.NEGATIVE_INFINITY}
      for(let panelIndex in panels) {
        let panel = panels[panelIndex]
        aabb.left = Math.min(aabb.left, panel.pos.x-.5)
        aabb.right = Math.max(aabb.right, panel.pos.x+.5)
        aabb.top = Math.min(aabb.top, panel.pos.y-.5)
        aabb.bottom = Math.max(aabb.bottom, panel.pos.y+.5)
      }
      return aabb
    },
    panelsOverviewScale(panels) {
      let aabb = this.panelsAABB(panels)
      let lrToOne = 2 / (aabb.right - aabb.left)
      let tbToOne = 2 / (aabb.bottom - aabb.top)
      return Math.min(lrToOne, tbToOne)
    },
    panelState(panelIndex, panels) {
      let hasConnection = false
      if(panelIndex == 0) {
        hasConnection = true
      }
      else {
        for(let i in panels) {
          let discovered = this.gameState.panelState[i].discovered
          let connected = panels[i].advance.indexOf(panelIndex) != -1
          if(discovered && connected) {
            hasConnection = true
            break
          }
        }
      }
      if(!hasConnection || this.gameState.panelState[panelIndex].hidden)
        return 'invisible'
      else if(this.gameState.panelState[panelIndex].locked)
        return 'locked'
      else if(!this.gameState.panelState[panelIndex].discovered)
        return 'undiscovered'
      else if(this.focusedPanel != panelIndex)
        return 'discovered'
      else
        return 'interactive'
    },
    panelDiscoveredPredecessor(panelIndex, panels) {
      for(let i in panels) {
        let discovered = this.gameState.panelState[i].discovered
        let connected = panels[i].advance.indexOf(panelIndex) != -1
        if(discovered && connected) {
          return i
        }
      }
      return -1
    }
  }
}
